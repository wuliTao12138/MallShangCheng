## 项目介绍
* mall是Java语言的电商平台
* 使用springboot框架开发
* 使用Maven对项目进行模块化管理，提高项目的易开发性、扩展性
* 系统包括6个模块：登录注册、商品展示、购物车、订单展示、支付宝支付、收货地址
* 公共功能：日志管理，缓存管理
* 安全管理：使用shiro管理用户认证与授

## 主要功能
- 数据库：Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能
- MVC： 基于springboot注解,Rest风格Controller。Exception统一管理
- 文件存储：使用七牛云cdn，实现高效文件存储
- 日志：log4j打印日志
- 支付宝：接入支付宝当面付功能
## 技术选型
+ 核心框架：Spring
+ 安全框架：Apache Shiro
+ 持久层框架：MyBatis
+ 数据库连接池：Alibaba Druid
+ 缓存框架：Redis
+ 日志管理：SLF4J、Log4j
+ 前端框架：Jquery+VUE
## 支付流程
## 项目截图
![输入图片说明](https://gitee.com/uploads/images/2017/1129/222216_14f2fbc6_1562646.png "QQ图片20171129222155.png")
![输入图片说明](https://gitee.com/uploads/images/2017/1129/222052_4fb83248_1562646.jpeg "22519489.jpg")