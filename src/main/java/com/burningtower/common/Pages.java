package com.burningtower.common;

import lombok.Data;

import java.util.List;

@Data
public class Pages<T> {
    private Integer pageNum;
    private Integer pageSize;
    private Integer size;
    private String orderBy;
    private Integer startRow;
    private Integer endRow;
    private Integer total;
    private Integer pages;
    private List<T> list;
    private Integer firstPage;
    private Integer prePage;
    private Integer nextPage;
    private Integer lastPage;
    private Boolean isFirstPage;
    private Boolean isLastPage;
    private Boolean hasPreviousPage;
    private Boolean hasNextPage;
    private Integer navigatePages;
    private Integer[] navigatepageNums;

    public Pages() {
    }

    public Pages(Integer pageNum, Integer pageSize, String orderBy, List<T> list, Integer total) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.orderBy = orderBy;
        this.list = list;
        this.total=total;
        startRow=1;
        endRow=total;
        pages=total/pageSize+(total%pageSize==0?0:1);
        size=pageNum<pages?pageSize:(total%pageSize);
        firstPage=1;
        prePage=pageNum==1?1:(pageNum-1);
        nextPage=pageNum==pages?pages:(pageNum+1);
        lastPage=pages;
        isFirstPage=pageNum==1;
        isLastPage=pageNum==pages;
        hasPreviousPage=!isFirstPage;
        hasNextPage=!isLastPage;
        navigatePages=8;
        navigatepageNums=new Integer[pages<8?pages:8];
        for(int i=0;i<navigatepageNums.length;i++){
            navigatepageNums[i]=i+1;
        }
    }
}
