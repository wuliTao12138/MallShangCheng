package com.burningtower.common;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Result <T>{

    private Integer status;
    private T data;
    private String msg;
}
