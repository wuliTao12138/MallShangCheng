package com.burningtower.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Cart {
  @Id
  @GeneratedValue
  private Integer id;
  private Integer userId;
  private Integer productId;
  private Integer quantity;
  private Integer checked;
  private Date createTime;
  private Date updateTime;
}
