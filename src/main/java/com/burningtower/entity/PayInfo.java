package com.burningtower.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class PayInfo {
  @Id
  @GeneratedValue
  private Integer id;
  private Integer userId;
  private Long orderNo;
  private Integer payPlatform;
  private String platformNumber;
  private String platformStatus;
  private Date createTime;
  private Date updateTime;
}
