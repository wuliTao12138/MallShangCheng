package com.burningtower.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
public class OrderItem {
  @Id
  @GeneratedValue
  private Integer id;
  private Integer userId;
  private Long orderNo;
  private Integer productId;
  private String productName;
  private String productImage;
  private BigDecimal currentUnitPrice;
  private Integer quantity;
  private BigDecimal totalPrice;
  private Date createTime;
  private Date updateTime;

}
