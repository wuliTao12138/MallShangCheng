package com.burningtower.entity;


import com.burningtower.Enum.GoodsEnum;
import lombok.Data;

/**
 * Created by Administrator on 2017/10/30.
 */
@Data
public class AjaxResult {
    private String msg;
    private Integer code;

    public AjaxResult() {
    }

    public AjaxResult(String msg, Integer code) {

        this.msg = msg;
        this.code = code;
    }
    public AjaxResult(GoodsEnum goodsEnum){
        this.code=goodsEnum.getCode();
        this.msg=goodsEnum.getMsg();
    }
}
