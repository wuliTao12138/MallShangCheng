package com.burningtower.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class User {
  @Id
  @GeneratedValue
  private Integer id;
  private String username;
  private String password;
  private String email;
  private String phone;
  private String question;
  private String answer;
  private Integer role;
  private Date createTime;
  private Date updateTime;
}
