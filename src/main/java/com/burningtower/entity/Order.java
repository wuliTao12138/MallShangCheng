package com.burningtower.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@Table(name = "orders")
public class Order {
  @Id
  @GeneratedValue
  private Integer id;
  private Long orderNo;
  private Integer userId;
  private Integer shippingId;
  private BigDecimal payment;
  private Integer paymentType;
  private Integer postage;
  private Integer status;
  private Date paymentTime;
  private Date sendTime;
  private Date endTime;
  private Date closeTime;
  private Date createTime;
  private Date updateTime;
}
