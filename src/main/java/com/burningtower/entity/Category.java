package com.burningtower.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Category {
  @Id
  @GeneratedValue
  private Integer id;
  private Integer parentId;
  private String name;
  private Integer status;
  private Integer sortOrder;
  private Date createTime;
  private Date updateTime;

}
