package com.burningtower.entity;

import lombok.Data;

/**
 * Created by Administrator on 2017/10/30.
 */
@Data
public class DatagridReq  {
   private Integer page;//当前页数
   private Integer rows;//每页记录数
   private String sort;//排序字段
   private String order;//排序方式
}
