package com.burningtower.entity;

import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2017/10/30.
 */
@Data
public class DatagridResult<T> {
    private Integer total;//这两个属性，必须叫这个名字，不然无法对号入座，虽然也能传到前台
    private List<T> rows;

    public DatagridResult() {
    }

    public DatagridResult(Integer total, List<T> rows) {

        this.total = total;
        this.rows = rows;
    }
}
