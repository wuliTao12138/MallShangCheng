package com.burningtower.enums;

public enum AliCallbackEnum {
    TRADE_SUCCESS("TRADE_SUCCESS"),
    WAIT_BUYER_PAY("WAIT_BUYER_PAY"),
    RESPONSE_SUCCESS("success"),
    RESPONSE_FAILED("failed");
    private String msg;

    AliCallbackEnum(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
