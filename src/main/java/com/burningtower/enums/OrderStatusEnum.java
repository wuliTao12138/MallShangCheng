package com.burningtower.enums;

public enum OrderStatusEnum {
    CANCELED(0,"已取消"),
    UNPAID(10,"未支付"),
    PAID(20,"已付款"),
    SHIPPED(40,"已发货"),
    SUCCESS(50,"订单完成"),
    CLOSED(60,"订单关闭");

    OrderStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
