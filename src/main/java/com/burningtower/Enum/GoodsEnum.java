package com.burningtower.Enum;

/**
 * Created by Administrator on 2017/10/30.
 */
public enum GoodsEnum {

    INSERT_SUC(100,"添加成功"),
    INSERT_FAIL(101,"添加失败"),
    DELETE_SUC(200,"删除成功"),
    DELETE_FAIL(201,"删除失败"),
    UPDATE_SUCCESS(300,"修改成功"),
    UPDATE_ERROR(301,"修改失败");
    private Integer code;
    private String msg;
    GoodsEnum(Integer code, String msg) {
        this.code=code;
        this.msg=msg;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {

        return code;
    }

    public String getMsg() {
        return msg;
    }
}
