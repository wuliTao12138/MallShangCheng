package com.burningtower.vo;

import com.burningtower.enums.LimitQuantity;
import lombok.Data;
import java.math.BigDecimal;

@Data
public class CartProductVo {
    private Integer id;
    private Integer userId;
    private Integer productId;
    private Integer quantity;
    private String productName;
    private String productSubtitle;
    private String productMainImage;
    private BigDecimal productPrice;
    private Integer productStatus;
    private BigDecimal productTotalPrice;
    private Integer productStock;
    private Integer productChecked;
    private Enum<LimitQuantity> limitQuantity;

}
