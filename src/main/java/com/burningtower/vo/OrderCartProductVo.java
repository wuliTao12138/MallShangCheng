package com.burningtower.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 查询购物车时返回
 */
@Data
public class OrderCartProductVo {
    private List<OrderItemVo> orderItemVoList;
    private String imageHost;
    private BigDecimal productTotalPrice;
}
