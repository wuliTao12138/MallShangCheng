package com.burningtower.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 查询列表和详情时返回
 */
@Data
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderVo {
    private Long orderNo;
    private BigDecimal payment;
    private Integer paymentType;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String paymentTypeDesc;
    private Integer postage;
    private Integer status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String statusDesc;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date paymentTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date sendTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date closeTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    private List<OrderItemVo> orderItemVoList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String imageHost;
    private Integer shippingId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String receiverName;
    private ShippingVo shippingVo;
}
