//package com.burningtower.controller;
//
//import com.baomidou.mybatisplus.mapper.EntityWrapper;
//import com.baomidou.mybatisplus.plugins.Page;
//import com.study.Enum.GoodsEnum;
//import com.study.entity.AjaxResult;
//import com.study.entity.DatagridReq;
//import com.study.entity.DatagridResult;
//import com.study.entity.Goods;
//import com.study.service.GoodsService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
///**
// * Created by Administrator on 2017/10/27.
// */
//@Controller
//@RequestMapping( value = "goods")//这里的value可以不写，之前写成了name发生点奇怪的事，直接开tomcat居然可以直接访问这里
//public class GoodsController {
//    @Autowired
//    private GoodsService goodsService;
//
//
//    @RequestMapping( value = "list")
//    @ResponseBody
//    public DatagridResult<Goods> list(DatagridReq req){
//        Page<Goods> page=goodsService.selectPage(new Page<Goods>(req.getPage(),req.getRows()),new EntityWrapper<Goods>().orderBy(req.getSort(),req.getOrder().equals("asc")?true:false));
//        DatagridResult<Goods> result=new DatagridResult<Goods>(page.getTotal(),page.getRecords());
//
//        return result;
//    }
//    @RequestMapping( value = "update")
//    public void updateById(@RequestBody Goods rowData){//收Jason的数据要加上requestbody的注解
//        System.out.println(rowData.getGid());
//        goodsService.updateById(rowData);
//    }
//    @RequestMapping( value = "add")
//    @ResponseBody
//    public void add( Goods rowData){
//        goodsService.insert(rowData);
//    }
//    @RequestMapping( value = "/{gid}/delete")
//    @ResponseBody
//    public AjaxResult delete( @PathVariable("gid") Long gid){
//        boolean b = goodsService.deleteById(gid);
//        /*return b?new AjaxResult("delete_success:", 200):new AjaxResult("del_error: ",201);*/
//         return b?new AjaxResult(GoodsEnum.DELETE_SUC):new AjaxResult(GoodsEnum.DELETE_FAIL);
//    }
//}
