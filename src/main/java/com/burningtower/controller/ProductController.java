package com.burningtower.controller;

import com.burningtower.common.Pages;
import com.burningtower.common.Result;
import com.burningtower.entity.Product;
import com.burningtower.service.ProductService;
import com.burningtower.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import com.burningtower.common.Pages;

/**
 * Created by Administrator on 2017/11/2.
 */
@RestController
@RequestMapping("product")
public class ProductController {
    @Autowired
    private ProductService productService;

    /**
     * 根据productId查询
     */
    @RequestMapping("detail.do")
    public Result<Product> get(Integer productId){
        Product product = productService.get(productId);

        Result<Product> result = new Result<>();
        result.setStatus(0);
        result.setData(product);
        return result;
    }

    /**
     * 模糊查询
     */
    @RequestMapping("list.do")
    public Result<Pages<ProductVo>> search(Integer categoryId, String keyword, String orderBy,
                                  //如果传入当前页数是null，对其初始化
                                  @RequestParam(required = false, defaultValue = "1")Integer pageNum,
                                  //如果传入size是null，对其初始化
                                  @RequestParam(required = false, defaultValue = "10") Integer pageSize){

        Pages<ProductVo> page =null;
        try{
            Product product=new Product();
            product.setCategoryId(categoryId);
            product.setName(keyword);
            page = productService.search( product, orderBy,  pageNum,  pageSize);//执行查询
        }catch (Exception e){
            throw new RuntimeException("参数错误");
        }

        Result<Pages<ProductVo>> result= new Result<>();
        result.setStatus(0);
        result.setData(page);
        return  result;
    }



}
