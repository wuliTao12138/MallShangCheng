package com.burningtower.controller;

import com.burningtower.common.Result;
import com.burningtower.entity.Cart;
import com.burningtower.service.CartService;
import com.burningtower.vo.CartVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("cart")
@RestController
public class CartController {
    @Autowired
    private CartService cartService;

    /**
     * 获取当前用户的购物车商品列表
     * @param session
     * @return
     */
    @RequestMapping("list.do")
    public Result<CartVo> list(HttpSession session){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        //获得Vo
        List<Cart> cartList = cartService.getListByUserId(userId);
        CartVo vo = cartService.toVo(cartList);

        Result<CartVo> result=new Result<>();
        result.setStatus(0);
        result.setData(vo);
        return result;
    }

    /**
     * 向购物车添加商品
     * @param session
     * @param productId
     * @param count
     * @return
     */
    @RequestMapping("add.do")
    public Result<CartVo> add(HttpSession session,
                              @RequestParam Integer productId,
                              @RequestParam Integer count){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        cartService.add(userId,productId,count);
        //获得Vo
        List<Cart> cartList = cartService.getListByUserId(userId);
        CartVo vo = cartService.toVo(cartList);

        Result<CartVo> result=new Result<>();
        result.setStatus(0);
        result.setData(vo);
        return result;
    }

    /**
     * 更改某商品数量
     * @param session
     * @param productId
     * @param count
     * @return
     */
    @RequestMapping("update.do")
    public Result<CartVo> update(HttpSession session,
                              @RequestParam Integer productId,
                              @RequestParam Integer count){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        cartService.update(userId,productId,count);
        //获得Vo
        List<Cart> cartList = cartService.getListByUserId(userId);
        CartVo vo = cartService.toVo(cartList);

        Result<CartVo> result=new Result<>();
        result.setStatus(0);
        result.setData(vo);
        return result;
    }

    /**
     * 删除购物车内某商品
     * @param session
     * @param productIds
     * @return
     */
    @RequestMapping("delete_product.do")
    public Result<CartVo> delete(HttpSession session,
                                 @RequestParam Integer[] productIds){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        cartService.delete(userId,productIds);
        //获得Vo
        List<Cart> cartList = cartService.getListByUserId(userId);
        CartVo vo = cartService.toVo(cartList);
        Result<CartVo> result=new Result<>();
        result.setStatus(0);
        result.setData(vo);
        return result;
    }

    /**
     * 选中购物车内某商品
     * @param session
     * @param productId
     * @return Result<CartVo>
     */
    @RequestMapping("select.do")
    public Result<CartVo> select(HttpSession session,
                                 @RequestParam Integer productId){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        cartService.select(userId,productId);
        //获得Vo
        List<Cart> cartList = cartService.getListByUserId(userId);
        CartVo vo = cartService.toVo(cartList);
        Result<CartVo> result=new Result<>();
        result.setStatus(0);
        result.setData(vo);
        return result;
    }

    /**
     * 取消选中购物车内某商品
     * @param session
     * @param productId
     * @return
     */
    @RequestMapping("un_select.do")
    public Result<CartVo> unSelect(HttpSession session,
                                 @RequestParam Integer productId){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        cartService.unSelect(userId,productId);
        //获得Vo
        List<Cart> cartList = cartService.getListByUserId(userId);
        CartVo vo = cartService.toVo(cartList);
        Result<CartVo> result=new Result<>();
        result.setStatus(0);
        result.setData(vo);
        return result;
    }

    /**
     * 获取购物车内商品总数
     * @param session
     * @return
     */
    @RequestMapping("get_cart_product_count.do")
    public Result<Integer> getCount(HttpSession session){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("未登录");
        }
        Result<Integer> result=new Result<>();
        result.setStatus(0);
        result.setData(cartService.getCount(userId));
        return result;
    }

    /**
     * 全选
     * @param session
     * @return
     */
    @RequestMapping("select_all.do")
    public Result<CartVo> selectAll(HttpSession session){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        cartService.selectAll(userId);
        //获得Vo
        List<Cart> cartList = cartService.getListByUserId(userId);
        CartVo vo = cartService.toVo(cartList);
        Result<CartVo> result=new Result<>();
        result.setStatus(0);
        result.setData(vo);
        return result;
    }

    /**
     * 全部取消选中
     * @param session
     * @return
     */
    @RequestMapping("un_select_all.do")
    public Result<CartVo> unSelectAll(HttpSession session){
        //获得userId
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        cartService.unSelectAll(userId);
        //获得Vo
        List<Cart> cartList = cartService.getListByUserId(userId);
        CartVo vo = cartService.toVo(cartList);
        Result<CartVo> result=new Result<>();
        result.setStatus(0);
        result.setData(vo);
        return result;
    }

}
