package com.burningtower.controller;

import com.burningtower.common.Pages;
import com.burningtower.common.Result;
import com.burningtower.entity.Shipping;
import com.burningtower.service.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("shipping")
public class ShippingController {
    @Autowired
    private ShippingService shippingService;

    /**
     * 新增收货地址
     * @param shipping
     * @return
     */
    @RequestMapping("add.do")
    public Result<Map<String,Integer>> add(HttpSession session,Shipping shipping){
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        shipping.setUserId(userId);
        Map<String,Integer> map=new HashMap<>();
        try{
            Integer shippingId = shippingService.add(shipping);//todo receiverDistrict
            map.put("shippingId",shippingId);
        }catch (Exception e){
            throw new RuntimeException("新建地址失败");
        }

        Result<Map<String,Integer>> result=new Result<>();
        result.setData(map);
        result.setStatus(0);
        result.setMsg("新建地址成功");
        return result;
    }

    /**
     * 删除地址
     * @param shippingId
     * @return
     */
    @RequestMapping("del.do")
    public Result del(HttpSession session,@NotNull Integer shippingId){
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        try{
            shippingService.delete(userId,shippingId);
        }catch (Exception e){
            throw new RuntimeException("删除地址失败");
        }
        Result result=new Result();
        result.setStatus(0);
        result.setMsg("删除地址成功");
        return result;
    }

    /**
     * 更改地址
     * @param shipping
     * @return
     */
    @RequestMapping("update.do")
    public Result update(HttpSession session,Shipping shipping){
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        shippingService.update(shipping);
        Result result=new Result();
        result.setStatus(0);
        result.setMsg("更新地址成功");
        return result;
    }

    /**
     * 查看选中地址
     * @param shippingId
     * @return
     */
    @RequestMapping("select.do")
    public Result<Shipping> select(HttpSession session,@NotNull Integer shippingId){
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        Result<Shipping> result=new Result<>();
        Shipping shipping = shippingService.getByUserIdAndId(userId,shippingId);
        if (shipping==null){
            throw new RuntimeException("未找到地址");
        }
        result.setData(shipping);
        result.setStatus(0);
        return result;
    }

    /**
     * 查看地址列表
     * @param session
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("list.do")
    public Result<Pages<Shipping>> list(HttpSession session,
                                       @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                       @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        Integer userId = (Integer) session.getAttribute("userId");
        if(userId==null){
            throw new RuntimeException("NEED_LOGIN");
        }
        Pages<Shipping> pages = shippingService.getPageByUserId(userId, pageNum, pageSize);
        Result<Pages<Shipping>> result=new Result<>();
        result.setStatus(0);
        result.setData(pages);
        return result;
    }
}
