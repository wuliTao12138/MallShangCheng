package com.burningtower.repo;

import com.burningtower.entity.PayInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayInfoRepository extends JpaRepository<PayInfo,Integer> {
}
