package com.burningtower.repo;

import com.burningtower.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by Administrator on 2017/11/2.
 */
public interface ProductRepository extends JpaRepository<Product,Integer>,JpaSpecificationExecutor<Product>{
     Page<Product> findByCategoryIdAndNameLike(Integer categoryId, String name, Pageable pageable);
     Page<Product> findByIdAndNameLike(Integer productId, String name, Pageable pageable);
     Page<Product> findByNameLike(String name, Pageable pageable);


}
