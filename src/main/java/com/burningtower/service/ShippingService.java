package com.burningtower.service;

import com.burningtower.common.Pages;
import com.burningtower.entity.Shipping;
import com.burningtower.vo.ShippingVo;

public interface ShippingService {
    ShippingVo getShippingVo(Integer shippingId);
    Integer add(Shipping shipping);
    void delete(Integer userId,Integer shippingId);
    void update(Shipping shipping);
    Shipping getByUserIdAndId(Integer userId, Integer id);
    Shipping get(Integer id);
    Pages<Shipping> getPageByUserId(Integer userId, Integer pageNum,Integer pageSize);
}
