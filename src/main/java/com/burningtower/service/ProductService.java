package com.burningtower.service;

import com.burningtower.common.Pages;
import com.burningtower.common.Result;
import com.burningtower.entity.Product;
import com.burningtower.vo.ProductVo;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2017/11/2.
 */
public interface ProductService {
    Product get(Integer productId);

    /**
     * 门户搜索
     *
     * @param product
     * @param orderBy
     * @param pageNum
     * @param pageSize
     * @return
     */
    Pages<ProductVo> search(Product product, String orderBy, Integer pageNum, Integer pageSize);

    /**
     * 后台搜索
     *
     * @param session
     * @param product
     * @param orderBy
     * @param pageNum
     * @param pageSize
     * @return
     */
    Pages<Product> adminSearch(HttpSession session, Product product, String orderBy, Integer pageNum, Integer pageSize);

    Result<String> add(Product product);

    Result<String> update(Product product);

    Result<String> updateStatus(Product product);

}
