package com.burningtower.service.impl;

import com.burningtower.entity.PayInfo;
import com.burningtower.repo.PayInfoRepository;
import com.burningtower.service.PayInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayInfoServiceImpl implements PayInfoService {
    @Autowired
    private PayInfoRepository repository;

    @Override
    public PayInfo add(PayInfo payInfo) {
        return repository.save(payInfo);
    }
}
