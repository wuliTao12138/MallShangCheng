package com.burningtower.service.impl;

import com.burningtower.common.Pages;
import com.burningtower.entity.Shipping;
import com.burningtower.repo.ShippingRepository;
import com.burningtower.service.ShippingService;
import com.burningtower.vo.ShippingVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Service
public class ShippingServiceImpl implements ShippingService {
    @Autowired
    private ShippingRepository repository;

    @Override
    public ShippingVo getShippingVo(@NotNull Integer shippingId) {
        Shipping shipping=new Shipping();
        shipping.setId(shippingId);
        Shipping one = repository.findOne(Example.of(shipping));
        ShippingVo vo=null;
        if(one!=null){
            vo=new ShippingVo();
            BeanUtils.copyProperties(one,vo);
        }
        return vo;
    }

    @Override
    public Integer add(@NotNull Shipping shipping) {
        shipping.setCreateTime(new Date());
        shipping.setUpdateTime(new Date());
        Shipping save = repository.save(shipping);
        return save.getId();
    }

    @Override
    public void delete(@NotNull Integer userId,@NotNull Integer shippingId) {
        Shipping one = repository.findOne(shippingId);
        if(one==null){
            throw new RuntimeException("未找到订单");
        }
        if(one.getUserId()!=userId){
            throw new RuntimeException("不是当前用户的订单");
        }
        repository.delete(shippingId);
    }

    @Override
    public void update(@NotNull Shipping shipping) {
        if(repository.exists(shipping.getId())) {//如果存在则修改，否则报错
            Shipping one = repository.findOne(shipping.getId());
            shipping.setCreateTime(one.getCreateTime());
            shipping.setUpdateTime(new Date());
            shipping.setUserId(one.getUserId());
            repository.save(shipping);
        }else{
            throw new RuntimeException("没找到收货地址");
        }
    }

    @Override
    public Shipping getByUserIdAndId(@NotNull Integer userId,@NotNull Integer id) {
        Shipping shipping=new Shipping();
        shipping.setUserId(userId);
        shipping.setId(id);
        return repository.findOne(Example.of(shipping));
    }

    @Override
    public Shipping get(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public Pages<Shipping> getPageByUserId(@NotNull Integer userId,
                                           @NotNull Integer pageNum,
                                           @NotNull Integer pageSize) {
        Shipping shipping=new Shipping();
        shipping.setUserId(userId);
        Pageable pageable=new PageRequest(pageNum-1,pageSize);
        Page<Shipping> page = repository.findAll(Example.of(shipping), pageable);
        Integer total = Math.toIntExact(page.getTotalElements());
        List<Shipping> shippingList = page.getContent();
        return new Pages<>(pageNum,pageSize,"createTime_desc",shippingList,total);
    }

}
