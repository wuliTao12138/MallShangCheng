package com.burningtower.service.impl;

import com.burningtower.entity.User;
import com.burningtower.repo.UserRepository;
import com.burningtower.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;


import javax.validation.constraints.NotNull;
import java.util.Date;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository repository;

    /**
     * 1.登录
     */
    @Override
    public User login( String username, String password) {
        User user=new User();
        user.setUsername(username);
        user.setPassword(password);
        return repository.findOne(Example.of(user));
    }

    /**
     * 2.注册
     */
    @Override
    public void add(@NotNull User user) {
        User u = new User();
        u.setUsername(user.getUsername());
        //判断输入的用户名是否存在
        if (repository.exists(Example.of(u))){
            throw new RuntimeException("用户已存在");
        }
        u.setPassword(user.getPassword());
        u.setEmail(user.getEmail());
        u.setPhone(user.getPhone());
        u.setQuestion(user.getQuestion());
        u.setAnswer(user.getAnswer());
        u.setRole(0);
        u.setCreateTime(new Date());
        u.setUpdateTime(new Date());
        repository.save(u);
    }

    /**
     * 3.检查用户名是否有效
     */
    @Override
    public Boolean checkUser(String str, String type) {
        User user = new User();
        //判断type等于username或者email时候的str
        if (type.equals("username")){
            user.setUsername(str);
        }else if (type.equals("email")){
            user.setEmail(str);
        }else{
            throw new RuntimeException("type属性错误!");
        }
        return repository.exists(Example.of(user));
    }

    /**
     * 4.获取登录用户信息
     */
    @Override
    public User getUserById(Integer id) {
        return repository.findOne(id);
    }

    /**
     * 5.忘记密码
     */
    @Override
    public User forgetPwd(String username) {
        User user=new User();
        user.setUsername(username);
        return repository.findOne(Example.of(user));
    }

    /**
     * 6.提交问题答案
     */
    @Override //提交问题答案
    public User forgetCheckAnswer(String username, String question, String answer) {
        User user = new User();
        user.setUsername(username);
        return repository.findOne(Example.of(user));
    }

    /**
     * 7.忘记密码的重设密码
     */
    @Override
    public void forgetResetPassword(String username, String passwordNew) {
        User user = new User();
        user.setUsername(username);
        User one = repository.findOne(Example.of(user));
        //重新设置密码
        one.setPassword(passwordNew);
        repository.save(one);
    }

    /**
     * 8.登录中状态重置密码
     */
    @Override
    public User resetPassword(Integer id, String passwordOld, String passwordNew) {
        User user = repository.findOne(id);
        //判断输入的密码是否与数据库旧密码相同
        if (!user.getPassword().equals(passwordOld)){
            throw new RuntimeException("旧密码输入错误");
        }
        //相同时重新设置密码
        user.setPassword(passwordNew);
        return repository.save(user);
    }

    /**
     * 9.登录状态更新个人信息
     */
    @Override
    public User updateInformation(Integer id, String email, String phone, String question, String answer) {
        User one = repository.findOne(id);
        one.setEmail(email);
        one.setPhone(phone);
        one.setQuestion(question);
        one.setAnswer(answer);
        one.setUpdateTime(new Date());
        return repository.save(one);
    }

    /**
     *10 获取当前登录用户的详细信息，并强制登录
     */
    @Override
    public User getInformation(Integer id) {
        User one = repository.findOne(id);
        one.setPassword("");
        return one;
    }


}
