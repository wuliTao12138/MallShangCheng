package com.burningtower.service;

import com.burningtower.entity.Cart;
import com.burningtower.vo.CartVo;

import java.util.List;

public interface CartService {
    /**
     * 选取用户所有购物车内物品
     * @param userId
     * @return
     */
    List<Cart> getListByUserId(Integer userId);

    /**
     * 将Cart转为CartVo
     * @param cartList
     * @return
     */
    CartVo toVo(List<Cart> cartList);

    /**
     * 添加一个
     * @param userId
     * @param productId
     * @param count
     * @return
     */
    Cart add(Integer userId, Integer productId, Integer count);

    /**
     * 更改购物车内某物品数量
     * @param userId
     * @param productId
     * @param count
     * @return
     */
    Cart update(Integer userId, Integer productId, Integer count);

    void delete(Integer id);
    /**
     * 删除购物车内一个物品
     * @param userId
     * @param productIds
     */
    void delete(Integer userId, Integer[] productIds);

    /**
     * 选定购物车内某商品
     * @param userId
     * @param productId
     */
    void select(Integer userId, Integer productId);

    /**
     * 取消选定购物车内某商品
     * @param userId
     * @param productId
     */
    void unSelect(Integer userId, Integer productId);

    /**
     * 获取购物车内商品总数
     * @param userId
     * @return
     */
    Integer getCount(Integer userId);

    /**
     * 选中所有购物车内商品
     * @param userId
     */
    void selectAll(Integer userId);

    /**
     * 取消选定所有购物车内商品
     * @param userId
     */
    void unSelectAll(Integer userId);
}
