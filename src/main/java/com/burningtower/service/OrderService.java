package com.burningtower.service;

import com.burningtower.common.Pages;
import com.burningtower.common.Result;
import com.burningtower.entity.Order;
import com.burningtower.vo.OrderVo;

import java.util.Map;

public interface OrderService {
    Order add(Integer userId, Integer shippingId);
    OrderVo toVo1(Order order);
    Order getOrderByUserIdAndOrderNo(Integer userId, Long orderNo);
    Order getOrderByOrderNo(Long orderNo);
    /**
     * 返回分页列表。其中orderBy的格式是"[参照字段]_[升降序]"，如"createTime_desc"
     * @param userId
     * @param pageNum
     * @param pageSize
     * @param orderBy
     * @return
     */
    Pages<OrderVo> getPagesByUserId(Integer userId, Integer pageNum, Integer pageSize, String orderBy);
    Pages<OrderVo> getPages(Integer pageNum, Integer pageSize, String orderBy);
    Pages<OrderVo> getPagesByOrderNo(Long orderNo, Integer pageNum, Integer pageSize, String orderBy);
    OrderVo toVo(Order order);
    void cancel(Integer userId,Long orderNo);
    /**
     * 根据用户和订单号进行支付，根据指定路径保存二维码
     * @param orderNo
     * @param userId
     * @param path
     * @return
     */
    Result<Map<String,String>> pay(Long orderNo, Integer userId, String path);

    /**
     * 根据主键修改Order
     * @param order
     */
    void update(Order order);

    /**
     * 验证支付宝回调信息，没问题才返回true
     * @param params
     * @return
     */
    Boolean aliCallback(Map<String,String>params);
}
