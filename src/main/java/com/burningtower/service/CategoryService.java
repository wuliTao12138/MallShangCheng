package com.burningtower.service;

import com.burningtower.common.Result;
import com.burningtower.entity.Category;

import java.util.List;

/**
 * Created by Administrator on 2017/11/6.
 */
public interface CategoryService {
    Result<List<Category>> getListById(Integer categoryId);
    Result<String> add(Integer parentId,String categoryName);
    Result<String> update(Integer categoryId ,String categoryName);
    Result<Integer []> getDeepById(Integer categoryId);
}
