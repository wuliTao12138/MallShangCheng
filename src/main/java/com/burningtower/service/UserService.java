package com.burningtower.service;

import com.burningtower.entity.User;




public interface UserService {

    User login(String username, String password);
    void add(User user);

    /**
     * 检查用户名是否有效，有效返回true
     * @param str
     * @param type
     * @return
     */
    Boolean checkUser(String str, String type);
    User getUserById(Integer id);
    User forgetPwd(String username);
    User forgetCheckAnswer(String username,String question,String answer);
    void forgetResetPassword(String username,String passwordNew);
    User resetPassword(Integer id, String passwordOld, String passwordNew);
    User updateInformation(Integer id, String email, String phone, String question, String answer);
    User getInformation(Integer id);
}
