package com.burningtower.service;

import com.burningtower.entity.PayInfo;

public interface PayInfoService {
    PayInfo add(PayInfo payInfo);
}
