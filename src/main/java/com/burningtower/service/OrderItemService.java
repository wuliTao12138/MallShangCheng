package com.burningtower.service;

import com.burningtower.entity.OrderItem;
import com.burningtower.vo.OrderItemVo;

import java.util.List;

public interface OrderItemService {
    OrderItem add(OrderItem orderItem);
    List<OrderItem> getListByOrderNo(Long orderNo);
    List<OrderItemVo> toVoList(List<OrderItem> orderItemList);

    /**
     * 根据UserId来获取当前购物车的商品，返回相应的orderItemVo的list
     * @param userId
     * @return
     */
    List<OrderItemVo> getVoListByUserId(Integer userId);


}
