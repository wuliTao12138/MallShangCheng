package com.burningtower.aop;

import com.burningtower.common.Result;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Component//设置为可被扫描到
@ControllerAdvice//控制器异常捕捉
public class GlobalExceptionHandler {
    @ExceptionHandler(value = Exception.class)//异常处理器
    @ResponseBody
    public Result handler(Exception e){//捕获何类型的异常
        e.printStackTrace();
        Result result=new Result();
        result.setMsg(e.getMessage());
        switch (e.getMessage()){
            case "NEED_LOGIN":
                result.setStatus(10);
                break;
            default:
                result.setStatus(1);
                break;
        }
        return result;//返回给前端
    }
}
