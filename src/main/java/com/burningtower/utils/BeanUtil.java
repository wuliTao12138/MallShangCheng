package com.burningtower.utils;

import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/11/3.
 */
public class BeanUtil {
    public <T,K> List<T> change(Class<T> t,List<K> list)  {
        List<T> listvo=new ArrayList<>();
        for(K p:list){
            T vo= null;
            try {
                vo = t.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            BeanUtils.copyProperties(p,vo);
            listvo.add(vo);
        }
        return listvo;
    }
}
