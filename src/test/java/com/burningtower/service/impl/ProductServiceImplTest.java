package com.burningtower.service.impl;

import com.burningtower.entity.Product;
import com.burningtower.repo.ProductRepository;
import com.burningtower.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2017/11/3.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductServiceImplTest {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductRepository productRepository;
//    @Test
//    public void mohu() throws Exception {
//        List<Product> list = productService.search(100002, "%Apple%");
//        BeanUtil bean = new BeanUtil();
//        List<ProductVo> list1 = bean.change(list);
//        System.out.println(list1);
//    }
    @Test
    public void test8(){
        /*Integer pages=0;
        Integer navigatePages=8;
        Integer[] navigatepageNums=new Integer[pages<8?pages:8];
        for(int i=0;i<8;i++){
            navigatepageNums[i]=i;
        }*/
        Pageable pageable=new PageRequest(0, 3,new Sort(Sort.Direction.DESC,"id"));
        Page<Product> plus = productRepository.findByCategoryIdAndNameLike(100002,"%Plus%",pageable);
        System.out.println(plus.getContent());
    }

}